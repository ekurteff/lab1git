
#include "ME218_PIC32_2020.h"
#include <xc.h>
#define DELAY_TIME 5000//used to set delay for hi/lo cycle. 5000 is approx 2ms.
//#define WRITEONLY



/*Function Prototypes*/
int main(void);
void pinMode(uint8_t pin, uint8_t mode);


void pinMode(uint8_t pin, uint8_t mode){
    switch (pin)
    {
        case 1: ANSELAbits.ANSA0 = 0;//Set digital
        TRISAbits.TRISA0 = mode;//Set I/O
        break;
        
        case 2: ANSELAbits.ANSA1 = 0;//Set digital
        TRISAbits.TRISA1 = mode;//Set I/O
        break;
        
        case 3:
        TRISAbits.TRISA2 = mode;//Set I/O
        break;
        
        case 4:
        TRISAbits.TRISA3 = mode;//Set I/O
        break;
        
        case 5:
        TRISAbits.TRISA4 = mode;//Set I/O
        break;
        
        case 6: ANSELBbits.ANSB0 = 0;//Set digital
        TRISBbits.TRISB0 = mode;//Set I/O
        break;
        
        case 7: ANSELBbits.ANSB1 = 0;//Set digital
        TRISBbits.TRISB1 = mode;//Set I/O
        break;
        
        case 8: ANSELBbits.ANSB2 = 0;//Set digital
        TRISBbits.TRISB2 = mode;//Set I/O
        break;
        
        case 9: ANSELBbits.ANSB3 = 0;//Set digital
        TRISBbits.TRISB3 = mode;//Set I/O
        break;
        
        case 10: 
        TRISBbits.TRISB4 = mode;//Set I/O
        break;
        
        case 11:
        TRISBbits.TRISB5 = mode;//Set I/O
        break;
        
        case 12: 
        TRISBbits.TRISB6 = mode;//Set I/O
        break;
        
        case 13: 
        TRISBbits.TRISB7 = mode;//Set I/O
        break;
        
        case 14: 
        TRISBbits.TRISB8 = mode;//Set I/O
        break;
        
        case 15:
        TRISBbits.TRISB9 = mode;//Set I/O
        break;
        
        case 16: 
        TRISBbits.TRISB10 = mode;//Set I/O
        break;
        
        case 17: 
        TRISBbits.TRISB11 = mode;//Set I/O
        break;
        
        case 18: ANSELBbits.ANSB12 = 0;//Set digital
        TRISBbits.TRISB12 = mode;//Set I/O
        break;
        
        case 19: ANSELBbits.ANSB13 = 0;//Set digital
        TRISBbits.TRISB13 = mode;//Set I/O
        break;
        
        case 20: ANSELBbits.ANSB14 = 0;//Set digital
        TRISBbits.TRISB14 = mode;//Set I/O
        break;
        
        case 21: ANSELBbits.ANSB15 = 0;//Set digital
        TRISBbits.TRISB15 = mode;//Set I/O
        break;
        
        default: break;
    }
}
void digitalWrite(uint8_t pin, uint8_t value){
    switch(pin)
    {
    case 1: LATAbits.LATA0 = value;//Set pin HIGH or LOW
    break;
    
    case 2: LATAbits.LATA1 = value;//Set pin HIGH or LOW
    break;
    
    case 3: LATAbits.LATA2 = value;//Set pin HIGH or LOW
    break;
    
    case 4: LATAbits.LATA3 = value;//Set pin HIGH or LOW
    break;
    
    case 5: LATAbits.LATA4 = value;//Set pin HIGH or LOW
    break;
    
    case 6: LATBbits.LATB0 = value;//Set pin HIGH or LOW
    break;
    
    case 7: LATBbits.LATB1 = value;//Set pin HIGH or LOW
    break;
    
    case 8: LATBbits.LATB2 = value;//Set pin HIGH or LOW
    break;
    
    case 9: LATBbits.LATB3 = value;//Set pin HIGH or LOW
    break;
    
    case 10: LATBbits.LATB4 = value;//Set pin HIGH or LOW
    break;
    
    case 11: LATBbits.LATB5 = value;//Set pin HIGH or LOW
    break;
    
    case 12: LATBbits.LATB6 = value;//Set pin HIGH or LOW
    break;
    
    case 13: LATBbits.LATB7 = value;//Set pin HIGH or LOW
    break;
    
    case 14: LATBbits.LATB8 = value;//Set pin HIGH or LOW
    break;
    
    case 15: LATBbits.LATB9 = value;//Set pin HIGH or LOW
    break;
    
    case 16: LATBbits.LATB10 = value;//Set pin HIGH or LOW
    break;
    
    case 17: LATBbits.LATB11 = value;//Set pin HIGH or LOW
    break;
    
    case 18: LATBbits.LATB12 = value;//Set pin HIGH or LOW
    break;
    
    case 19: LATBbits.LATB13 = value;//Set pin HIGH or LOW
    break;
    
    case 20: LATBbits.LATB14 = value;//Set pin HIGH or LOW
    break;
    
    case 21: LATBbits.LATB15 = value;//Set pin HIGH or LOW
    break;
    
    default: break;
    }
}
uint8_t digitalRead(uint8_t pin){ //Read pin state as 0 or 1
    uint8_t pinVal = 0;//Declare pin value to be returned
    
    switch(pin)
    {
    case 1: pinVal = PORTAbits.RA0;//Read from pin
    break;
    
    case 2: pinVal = PORTAbits.RA1;//Read from pin
    break;
    
    case 3: pinVal = PORTAbits.RA2;//Read from pin
    break;
    
    case 4: pinVal = PORTAbits.RA3;//Read from pin
    break;
    
    case 5: pinVal = PORTAbits.RA4;//Read from pin
    break;
    
    case 6: pinVal = PORTBbits.RB0;//Read from pin
    break;
    
    case 7: pinVal = PORTBbits.RB1;//Read from pin
    break;
    
    case 8: pinVal = PORTBbits.RB2;//Read from pin
    break;
    
    case 9: pinVal = PORTBbits.RB3;//Read from pin
    break;
    
    case 10: pinVal = PORTBbits.RB4;//Read from pin
    break;
    
    case 11: pinVal = PORTBbits.RB5;//Read from pin
    break;
    
    case 12: pinVal = PORTBbits.RB6;//Read from pin
    break;
    
    case 13: pinVal = PORTBbits.RB7;//Read from pin
    break;
    
    case 14: pinVal = PORTBbits.RB8;//Read from pin
    break;
    
    case 15: pinVal = PORTBbits.RB9;//Read from pin
    break;
    
    case 16: pinVal = PORTBbits.RB10;//Read from pin
    break;
    
    case 17: pinVal = PORTBbits.RB11;//Read from pin
    break;
    
    case 18: pinVal = PORTBbits.RB12;//Read from pin
    break;
    
    case 19: pinVal = PORTBbits.RB13;//Read from pin
    break;
    
    case 20: pinVal = PORTBbits.RB14;//Read from pin
    break;
    
    case 21: pinVal = PORTBbits.RB15;//Read from pin
    break;
    
    default: break;
    }
    return pinVal;
    
    
}
int main(void) {
#ifdef WRITEONLY //Test a single pin for pinMode() and digitalWrite()
    /*Set PinModes*/
    uint8_t testPin = 9;
    pinMode(testPin, 0);//Set testPin as output

        
    while(1) //Loop forever
    {
        int i = 0;//Declare for use in loop
        int j = 0;//Incremented to cause delay
      
        digitalWrite(testPin,1);    //testPin set to HIGH
        for(i=0; i<DELAY_TIME; i++)//Force a delay
        {
            j++;
        }
        
        digitalWrite(testPin,0);    //testPin  set to LOW
        for(i=0; i<DELAY_TIME; i++)//Force a delay
        {
            j++;
        }
    }
#else //Test all pins for input
     uint8_t outPin = 1;//Outputs the read state of testPin
     uint8_t modePin = 2;//Reads touch switch output
     uint8_t testPin = 3;//Pin to be tested for input
     
     /*Set I/0*/
     pinMode(outPin, 0);//Set outPin to output
     pinMode(modePin, 1);//Set modePin to input
     
    while(1)//loop forever
    {
        static uint8_t testVal = 0; //Declare local var to store testPin value
        static uint8_t modeVal = 0; //Declare local var to store mode value
        static uint8_t lastModeVal = 0; //Declare local var to track last mode value
        
        modeVal = digitalRead(modePin);//Read modePin
        pinMode(testPin, 1);//Set testPin to input
        testVal = digitalRead(testPin);//Read testPin
        digitalWrite(outPin,testVal);//Set outPin to testPin
        
        if(modeVal == 0 && lastModeVal == 1)//Check for falling edge on mode pin
        {
            testPin++;//Advance to next pin
        }
        
        if(testPin > 21)//If we exceed the last pin
        {
            testPin = 3;//Loop back around to pin 3
        }
        
        lastModeVal = modeVal;
    }
#endif
     
     

}
    
    




