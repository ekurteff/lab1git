	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 3
	.section	.text,code
.Ltext0:
	.align	2
	.globl	pinMode
.LFB7 = .
	.file 1 "u:/ekurteff/my documents/lab1/lab1git/lab1/simmain.c"
	.loc 1 14 0
	.set	nomips16
	.set	nomicromips
	.ent	pinMode
	.type	pinMode, @function
pinMode:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
.LVL0 = .
	addiu	$sp,$sp,-8
.LCFI0 = .
	sw	$fp,4($sp)
.LCFI1 = .
	move	$fp,$sp
.LCFI2 = .
	move	$3,$4
	move	$2,$5
	sb	$3,8($fp)
	sb	$2,12($fp)
	.loc 1 15 0
	lbu	$2,8($fp)
	sltu	$3,$2,22
	beq	$3,$0,.L26
	nop

	sll	$3,$2,2
	lui	$2,%hi(.L4)
	addiu	$2,$2,%lo(.L4)
	addu	$2,$3,$2
	lw	$2,0($2)
	j	$2
	nop

	.align	2
	.align	2
.L4:
	.word	.L26
	.word	.L3
	.word	.L5
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L14
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
.L3:
	.loc 1 17 0
	lui	$3,%hi(ANSELA)
	lbu	$2,%lo(ANSELA)($3)
	ins	$2,$0,0,1
	sb	$2,%lo(ANSELA)($3)
	.loc 1 18 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL1 = .
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,0,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 19 0
	j	.L1
	nop

.LVL2 = .
.L5:
	.loc 1 21 0
	lui	$3,%hi(ANSELA)
	lbu	$2,%lo(ANSELA)($3)
	ins	$2,$0,1,1
	sb	$2,%lo(ANSELA)($3)
	.loc 1 22 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL3 = .
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,1,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 23 0
	j	.L1
	nop

.LVL4 = .
.L6:
	.loc 1 26 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL5 = .
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,2,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 27 0
	j	.L1
	nop

.LVL6 = .
.L7:
	.loc 1 30 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL7 = .
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,3,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 31 0
	j	.L1
	nop

.LVL8 = .
.L8:
	.loc 1 34 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL9 = .
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,4,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 35 0
	j	.L1
	nop

.LVL10 = .
.L9:
	.loc 1 37 0
	lui	$3,%hi(ANSELB)
	lhu	$2,%lo(ANSELB)($3)
	ins	$2,$0,0,1
	sh	$2,%lo(ANSELB)($3)
	.loc 1 38 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL11 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,0,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 39 0
	j	.L1
	nop

.LVL12 = .
.L10:
	.loc 1 41 0
	lui	$3,%hi(ANSELB)
	lhu	$2,%lo(ANSELB)($3)
	ins	$2,$0,1,1
	sh	$2,%lo(ANSELB)($3)
	.loc 1 42 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL13 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,1,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 43 0
	j	.L1
	nop

.LVL14 = .
.L11:
	.loc 1 45 0
	lui	$3,%hi(ANSELB)
	lhu	$2,%lo(ANSELB)($3)
	ins	$2,$0,2,1
	sh	$2,%lo(ANSELB)($3)
	.loc 1 46 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL15 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,2,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 47 0
	j	.L1
	nop

.LVL16 = .
.L12:
	.loc 1 49 0
	lui	$3,%hi(ANSELB)
	lhu	$2,%lo(ANSELB)($3)
	ins	$2,$0,3,1
	sh	$2,%lo(ANSELB)($3)
	.loc 1 50 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL17 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,3,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 51 0
	j	.L1
	nop

.LVL18 = .
.L13:
	.loc 1 54 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL19 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,4,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 55 0
	j	.L1
	nop

.LVL20 = .
.L14:
	.loc 1 58 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL21 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,5,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 59 0
	j	.L1
	nop

.LVL22 = .
.L15:
	.loc 1 62 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL23 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,6,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 63 0
	j	.L1
	nop

.LVL24 = .
.L16:
	.loc 1 66 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL25 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,7,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 67 0
	j	.L1
	nop

.LVL26 = .
.L17:
	.loc 1 70 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL27 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,8,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 71 0
	j	.L1
	nop

.LVL28 = .
.L18:
	.loc 1 74 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL29 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,9,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 75 0
	j	.L1
	nop

.LVL30 = .
.L19:
	.loc 1 78 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL31 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,10,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 79 0
	j	.L1
	nop

.LVL32 = .
.L20:
	.loc 1 82 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL33 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,11,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 83 0
	j	.L1
	nop

.LVL34 = .
.L21:
	.loc 1 86 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL35 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,12,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 87 0
	j	.L1
	nop

.LVL36 = .
.L22:
	.loc 1 90 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL37 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,13,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 91 0
	j	.L1
	nop

.LVL38 = .
.L23:
	.loc 1 94 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL39 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,14,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 95 0
	j	.L1
	nop

.LVL40 = .
.L24:
	.loc 1 98 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL41 = .
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,15,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 99 0
	j	.L1
	nop

.LVL42 = .
.L26:
	.loc 1 101 0
	nop
.LVL43 = .
.L1:
	.loc 1 103 0
	move	$sp,$fp
.LCFI3 = .
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
.LCFI4 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	pinMode
.LFE7:
	.size	pinMode, .-pinMode
	.align	2
	.globl	digitalWrite
.LFB8 = .
	.loc 1 104 0
	.set	nomips16
	.set	nomicromips
	.ent	digitalWrite
	.type	digitalWrite, @function
digitalWrite:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
.LVL44 = .
	addiu	$sp,$sp,-8
.LCFI5 = .
	sw	$fp,4($sp)
.LCFI6 = .
	move	$fp,$sp
.LCFI7 = .
	move	$3,$4
	move	$2,$5
	sb	$3,8($fp)
	sb	$2,12($fp)
	.loc 1 105 0
	lbu	$2,8($fp)
	sltu	$3,$2,22
	beq	$3,$0,.L52
	nop

	sll	$3,$2,2
	lui	$2,%hi(.L30)
	addiu	$2,$2,%lo(.L30)
	addu	$2,$3,$2
	lw	$2,0($2)
	j	$2
	nop

	.align	2
	.align	2
.L30:
	.word	.L52
	.word	.L29
	.word	.L31
	.word	.L32
	.word	.L33
	.word	.L34
	.word	.L35
	.word	.L36
	.word	.L37
	.word	.L38
	.word	.L39
	.word	.L40
	.word	.L41
	.word	.L42
	.word	.L43
	.word	.L44
	.word	.L45
	.word	.L46
	.word	.L47
	.word	.L48
	.word	.L49
	.word	.L50
.L29:
	.loc 1 107 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL45 = .
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,0,1
	sb	$2,%lo(LATA)($3)
	.loc 1 108 0
	j	.L27
	nop

.LVL46 = .
.L31:
	.loc 1 110 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL47 = .
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,1,1
	sb	$2,%lo(LATA)($3)
	.loc 1 111 0
	j	.L27
	nop

.LVL48 = .
.L32:
	.loc 1 113 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL49 = .
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,2,1
	sb	$2,%lo(LATA)($3)
	.loc 1 114 0
	j	.L27
	nop

.LVL50 = .
.L33:
	.loc 1 116 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL51 = .
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,3,1
	sb	$2,%lo(LATA)($3)
	.loc 1 117 0
	j	.L27
	nop

.LVL52 = .
.L34:
	.loc 1 119 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL53 = .
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,4,1
	sb	$2,%lo(LATA)($3)
	.loc 1 120 0
	j	.L27
	nop

.LVL54 = .
.L35:
	.loc 1 122 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL55 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,0,1
	sh	$2,%lo(LATB)($3)
	.loc 1 123 0
	j	.L27
	nop

.LVL56 = .
.L36:
	.loc 1 125 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL57 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,1,1
	sh	$2,%lo(LATB)($3)
	.loc 1 126 0
	j	.L27
	nop

.LVL58 = .
.L37:
	.loc 1 128 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL59 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,2,1
	sh	$2,%lo(LATB)($3)
	.loc 1 129 0
	j	.L27
	nop

.LVL60 = .
.L38:
	.loc 1 131 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL61 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,3,1
	sh	$2,%lo(LATB)($3)
	.loc 1 132 0
	j	.L27
	nop

.LVL62 = .
.L39:
	.loc 1 134 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL63 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,4,1
	sh	$2,%lo(LATB)($3)
	.loc 1 135 0
	j	.L27
	nop

.LVL64 = .
.L40:
	.loc 1 137 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL65 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,5,1
	sh	$2,%lo(LATB)($3)
	.loc 1 138 0
	j	.L27
	nop

.LVL66 = .
.L41:
	.loc 1 140 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL67 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,6,1
	sh	$2,%lo(LATB)($3)
	.loc 1 141 0
	j	.L27
	nop

.LVL68 = .
.L42:
	.loc 1 143 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL69 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,7,1
	sh	$2,%lo(LATB)($3)
	.loc 1 144 0
	j	.L27
	nop

.LVL70 = .
.L43:
	.loc 1 146 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL71 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,8,1
	sh	$2,%lo(LATB)($3)
	.loc 1 147 0
	j	.L27
	nop

.LVL72 = .
.L44:
	.loc 1 149 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL73 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,9,1
	sh	$2,%lo(LATB)($3)
	.loc 1 150 0
	j	.L27
	nop

.LVL74 = .
.L45:
	.loc 1 152 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL75 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,10,1
	sh	$2,%lo(LATB)($3)
	.loc 1 153 0
	j	.L27
	nop

.LVL76 = .
.L46:
	.loc 1 155 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL77 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,11,1
	sh	$2,%lo(LATB)($3)
	.loc 1 156 0
	j	.L27
	nop

.LVL78 = .
.L47:
	.loc 1 158 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL79 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,12,1
	sh	$2,%lo(LATB)($3)
	.loc 1 159 0
	j	.L27
	nop

.LVL80 = .
.L48:
	.loc 1 161 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL81 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,13,1
	sh	$2,%lo(LATB)($3)
	.loc 1 162 0
	j	.L27
	nop

.LVL82 = .
.L49:
	.loc 1 164 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL83 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,14,1
	sh	$2,%lo(LATB)($3)
	.loc 1 165 0
	j	.L27
	nop

.LVL84 = .
.L50:
	.loc 1 167 0
	lbu	$2,12($fp)
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
.LVL85 = .
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,15,1
	sh	$2,%lo(LATB)($3)
	.loc 1 168 0
	j	.L27
	nop

.LVL86 = .
.L52:
	.loc 1 170 0
	nop
.LVL87 = .
.L27:
	.loc 1 172 0
	move	$sp,$fp
.LCFI8 = .
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
.LCFI9 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	digitalWrite
.LFE8:
	.size	digitalWrite, .-digitalWrite
	.align	2
	.globl	digitalRead
.LFB9 = .
	.loc 1 173 0
	.set	nomips16
	.set	nomicromips
	.ent	digitalRead
	.type	digitalRead, @function
digitalRead:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
.LVL88 = .
	addiu	$sp,$sp,-16
.LCFI10 = .
	sw	$fp,12($sp)
.LCFI11 = .
	move	$fp,$sp
.LCFI12 = .
	move	$2,$4
	sb	$2,16($fp)
	.loc 1 174 0
	sb	$0,0($fp)
.LVL89 = .
	.loc 1 176 0
	lbu	$2,16($fp)
	sltu	$3,$2,22
	beq	$3,$0,.L79
	nop

	sll	$3,$2,2
	lui	$2,%hi(.L56)
	addiu	$2,$2,%lo(.L56)
	addu	$2,$3,$2
	lw	$2,0($2)
	j	$2
	nop

	.align	2
	.align	2
.L56:
	.word	.L79
	.word	.L55
	.word	.L57
	.word	.L58
	.word	.L59
	.word	.L60
	.word	.L61
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
	.word	.L66
	.word	.L67
	.word	.L68
	.word	.L69
	.word	.L70
	.word	.L71
	.word	.L72
	.word	.L73
	.word	.L74
	.word	.L75
	.word	.L76
.L55:
	.loc 1 178 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,0,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL90 = .
	.loc 1 179 0
	j	.L77
	nop

.LVL91 = .
.L57:
	.loc 1 181 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,1,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL92 = .
	.loc 1 182 0
	j	.L77
	nop

.LVL93 = .
.L58:
	.loc 1 184 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,2,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL94 = .
	.loc 1 185 0
	j	.L77
	nop

.LVL95 = .
.L59:
	.loc 1 187 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,3,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL96 = .
	.loc 1 188 0
	j	.L77
	nop

.LVL97 = .
.L60:
	.loc 1 190 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,4,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL98 = .
	.loc 1 191 0
	j	.L77
	nop

.LVL99 = .
.L61:
	.loc 1 193 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,0,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL100 = .
	.loc 1 194 0
	j	.L77
	nop

.LVL101 = .
.L62:
	.loc 1 196 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,1,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL102 = .
	.loc 1 197 0
	j	.L77
	nop

.LVL103 = .
.L63:
	.loc 1 199 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,2,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL104 = .
	.loc 1 200 0
	j	.L77
	nop

.LVL105 = .
.L64:
	.loc 1 202 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,3,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL106 = .
	.loc 1 203 0
	j	.L77
	nop

.LVL107 = .
.L65:
	.loc 1 205 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,4,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL108 = .
	.loc 1 206 0
	j	.L77
	nop

.LVL109 = .
.L66:
	.loc 1 208 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,5,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL110 = .
	.loc 1 209 0
	j	.L77
	nop

.LVL111 = .
.L67:
	.loc 1 211 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,6,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL112 = .
	.loc 1 212 0
	j	.L77
	nop

.LVL113 = .
.L68:
	.loc 1 214 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,7,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL114 = .
	.loc 1 215 0
	j	.L77
	nop

.LVL115 = .
.L69:
	.loc 1 217 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,8,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL116 = .
	.loc 1 218 0
	j	.L77
	nop

.LVL117 = .
.L70:
	.loc 1 220 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,9,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL118 = .
	.loc 1 221 0
	j	.L77
	nop

.LVL119 = .
.L71:
	.loc 1 223 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,10,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL120 = .
	.loc 1 224 0
	j	.L77
	nop

.LVL121 = .
.L72:
	.loc 1 226 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,11,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL122 = .
	.loc 1 227 0
	j	.L77
	nop

.LVL123 = .
.L73:
	.loc 1 229 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,12,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL124 = .
	.loc 1 230 0
	j	.L77
	nop

.LVL125 = .
.L74:
	.loc 1 232 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,13,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL126 = .
	.loc 1 233 0
	j	.L77
	nop

.LVL127 = .
.L75:
	.loc 1 235 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,14,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL128 = .
	.loc 1 236 0
	j	.L77
	nop

.LVL129 = .
.L76:
	.loc 1 238 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,15,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
.LVL130 = .
	.loc 1 239 0
	j	.L77
	nop

.LVL131 = .
.L79:
	.loc 1 241 0
	nop
.L77:
	.loc 1 243 0
	lbu	$2,0($fp)
	.loc 1 246 0
	move	$sp,$fp
.LCFI13 = .
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
.LCFI14 = .
.LVL132 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	digitalRead
.LFE9:
	.size	digitalRead, .-digitalRead
	.align	2
	.globl	main
.LFB10 = .
	.loc 1 247 0
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$fp,32,$31		# vars= 8, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
	addiu	$sp,$sp,-32
.LCFI15 = .
	sw	$31,28($sp)
	sw	$fp,24($sp)
.LCFI16 = .
	move	$fp,$sp
.LCFI17 = .
	.loc 1 272 0
	li	$2,1			# 0x1
	sb	$2,17($fp)
.LVL133 = .
	.loc 1 273 0
	li	$2,2			# 0x2
.LVL134 = .
	sb	$2,18($fp)
.LVL135 = .
	.loc 1 274 0
	li	$2,3			# 0x3
.LVL136 = .
	sb	$2,16($fp)
.LVL137 = .
	.loc 1 277 0
	lbu	$2,17($fp)
.LVL138 = .
	move	$4,$2
	move	$5,$0
	jal	pinMode
	nop

.LVL139 = .
	.loc 1 278 0
	lbu	$2,18($fp)
	move	$4,$2
	li	$5,1			# 0x1
	jal	pinMode
	nop

.LVL140 = .
.L83:
.LBB2 = .
	.loc 1 286 0
	lbu	$2,18($fp)
	move	$4,$2
	jal	digitalRead
	nop

.LVL141 = .
	sb	$2,%gp_rel(modeVal.6918)($28)
	.loc 1 287 0
	lbu	$2,16($fp)
	move	$4,$2
	li	$5,1			# 0x1
	jal	pinMode
	nop

.LVL142 = .
	.loc 1 288 0
	lbu	$2,16($fp)
	move	$4,$2
	jal	digitalRead
	nop

.LVL143 = .
	sb	$2,%gp_rel(testVal.6917)($28)
	.loc 1 289 0
	lbu	$2,%gp_rel(testVal.6917)($28)
	lbu	$3,17($fp)
	move	$4,$3
	move	$5,$2
	jal	digitalWrite
	nop

.LVL144 = .
	.loc 1 291 0
	lbu	$2,%gp_rel(modeVal.6918)($28)
	bne	$2,$0,.L81
	nop

	lbu	$3,%gp_rel(lastModeVal.6919)($28)
	li	$2,1			# 0x1
	bne	$3,$2,.L81
	nop

	.loc 1 293 0
	lbu	$2,16($fp)
	addiu	$2,$2,1
	sb	$2,16($fp)
.LVL145 = .
.L81:
	.loc 1 296 0
	lbu	$2,16($fp)
	sltu	$2,$2,22
	bne	$2,$0,.L82
	nop

	.loc 1 298 0
	li	$2,3			# 0x3
	sb	$2,16($fp)
.LVL146 = .
.L82:
	.loc 1 301 0
	lbu	$2,%gp_rel(modeVal.6918)($28)
	sb	$2,%gp_rel(lastModeVal.6919)($28)
.LBE2 = .
	.loc 1 302 0
	j	.L83
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	main
.LFE10:
	.size	main, .-main
	.section	.sbss,bss
	.type	modeVal.6918, @object
	.size	modeVal.6918, 1
modeVal.6918:
	.space	1
	.type	testVal.6917, @object
	.size	testVal.6917, 1
testVal.6917:
	.space	1
	.type	lastModeVal.6919, @object
	.size	lastModeVal.6919, 1
lastModeVal.6919:
	.space	1
	.section	.debug_frame,info
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0x1f
	.byte	0xc
	.uleb128 0x1d
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI0-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI5-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI10-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI15-.LFB10
	.byte	0xe
	.uleb128 0x20
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0x9f
	.uleb128 0x1
	.byte	0x9e
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xd
	.uleb128 0x1e
	.align	2
.LEFDE6:
	.section	.text,code
.Letext0:
	.file 2 "c:/program files/microchip/xc32/v2.41/pic32mx/include/lega-c/machine/int_types.h"
	.file 3 "c:/program files (x86)/microchip/mplabx/v5.40/packs/microchip/pic32mx_dfp/1.3.231/include/proc/p32mx170f256b.h"
	.section	.debug_info,info
.Ldebug_info0:
	.4byte	0xc55
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.ascii	"GNU C 4.8.3 MPLAB XC32 Compiler v2.41\000"
	.byte	0x1
	.ascii	"SimMain.c\000"
	.ascii	"U:/ekurteff/My Documents/Lab1/Lab1Git/Lab1\000"
	.4byte	.Ltext0
	.4byte	.Letext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.ascii	"signed char\000"
	.uleb128 0x3
	.ascii	"__uint8_t\000"
	.byte	0x2
	.byte	0x2f
	.4byte	0x94
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.ascii	"unsigned char\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.ascii	"short int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.ascii	"short unsigned int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.ascii	"__uint32_t\000"
	.byte	0x2
	.byte	0x33
	.4byte	0xe1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.ascii	"unsigned int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.ascii	"long long int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.ascii	"long long unsigned int\000"
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x121b
	.4byte	0x14e
	.uleb128 0x5
	.ascii	"ANSA0\000"
	.byte	0x3
	.2byte	0x121c
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSA1\000"
	.byte	0x3
	.2byte	0x121d
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x121f
	.4byte	0x168
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1220
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x121a
	.4byte	0x17c
	.uleb128 0x7
	.4byte	0x11c
	.uleb128 0x7
	.4byte	0x14e
	.byte	0
	.uleb128 0x8
	.ascii	"__ANSELAbits_t\000"
	.byte	0x3
	.2byte	0x1222
	.4byte	0x168
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x122a
	.4byte	0x206
	.uleb128 0x5
	.ascii	"TRISA0\000"
	.byte	0x3
	.2byte	0x122b
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA1\000"
	.byte	0x3
	.2byte	0x122c
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA2\000"
	.byte	0x3
	.2byte	0x122d
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA3\000"
	.byte	0x3
	.2byte	0x122e
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA4\000"
	.byte	0x3
	.2byte	0x122f
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1231
	.4byte	0x220
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1232
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x1229
	.4byte	0x234
	.uleb128 0x7
	.4byte	0x193
	.uleb128 0x7
	.4byte	0x206
	.byte	0
	.uleb128 0x8
	.ascii	"__TRISAbits_t\000"
	.byte	0x3
	.2byte	0x1234
	.4byte	0x220
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x123c
	.4byte	0x2ae
	.uleb128 0x5
	.ascii	"RA0\000"
	.byte	0x3
	.2byte	0x123d
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA1\000"
	.byte	0x3
	.2byte	0x123e
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA2\000"
	.byte	0x3
	.2byte	0x123f
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA3\000"
	.byte	0x3
	.2byte	0x1240
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA4\000"
	.byte	0x3
	.2byte	0x1241
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1243
	.4byte	0x2c8
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1244
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x123b
	.4byte	0x2dc
	.uleb128 0x7
	.4byte	0x24a
	.uleb128 0x7
	.4byte	0x2ae
	.byte	0
	.uleb128 0x8
	.ascii	"__PORTAbits_t\000"
	.byte	0x3
	.2byte	0x1246
	.4byte	0x2c8
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x124e
	.4byte	0x360
	.uleb128 0x5
	.ascii	"LATA0\000"
	.byte	0x3
	.2byte	0x124f
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA1\000"
	.byte	0x3
	.2byte	0x1250
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA2\000"
	.byte	0x3
	.2byte	0x1251
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA3\000"
	.byte	0x3
	.2byte	0x1252
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA4\000"
	.byte	0x3
	.2byte	0x1253
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1255
	.4byte	0x37a
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1256
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x124d
	.4byte	0x38e
	.uleb128 0x7
	.4byte	0x2f2
	.uleb128 0x7
	.4byte	0x360
	.byte	0
	.uleb128 0x8
	.ascii	"__LATAbits_t\000"
	.byte	0x3
	.2byte	0x1258
	.4byte	0x37a
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12cb
	.4byte	0x451
	.uleb128 0x5
	.ascii	"ANSB0\000"
	.byte	0x3
	.2byte	0x12cc
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB1\000"
	.byte	0x3
	.2byte	0x12cd
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB2\000"
	.byte	0x3
	.2byte	0x12ce
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB3\000"
	.byte	0x3
	.2byte	0x12cf
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB12\000"
	.byte	0x3
	.2byte	0x12d1
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB13\000"
	.byte	0x3
	.2byte	0x12d2
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB14\000"
	.byte	0x3
	.2byte	0x12d3
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"ANSB15\000"
	.byte	0x3
	.2byte	0x12d4
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12d6
	.4byte	0x46b
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x12d7
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x12ca
	.4byte	0x47f
	.uleb128 0x7
	.4byte	0x3a3
	.uleb128 0x7
	.4byte	0x451
	.byte	0
	.uleb128 0x8
	.ascii	"__ANSELBbits_t\000"
	.byte	0x3
	.2byte	0x12d9
	.4byte	0x46b
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12e1
	.4byte	0x5f6
	.uleb128 0x5
	.ascii	"TRISB0\000"
	.byte	0x3
	.2byte	0x12e2
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB1\000"
	.byte	0x3
	.2byte	0x12e3
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB2\000"
	.byte	0x3
	.2byte	0x12e4
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB3\000"
	.byte	0x3
	.2byte	0x12e5
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB4\000"
	.byte	0x3
	.2byte	0x12e6
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB5\000"
	.byte	0x3
	.2byte	0x12e7
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB6\000"
	.byte	0x3
	.2byte	0x12e8
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB7\000"
	.byte	0x3
	.2byte	0x12e9
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB8\000"
	.byte	0x3
	.2byte	0x12ea
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB9\000"
	.byte	0x3
	.2byte	0x12eb
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB10\000"
	.byte	0x3
	.2byte	0x12ec
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB11\000"
	.byte	0x3
	.2byte	0x12ed
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB12\000"
	.byte	0x3
	.2byte	0x12ee
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB13\000"
	.byte	0x3
	.2byte	0x12ef
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB14\000"
	.byte	0x3
	.2byte	0x12f0
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB15\000"
	.byte	0x3
	.2byte	0x12f1
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12f3
	.4byte	0x610
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x12f4
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x12e0
	.4byte	0x624
	.uleb128 0x7
	.4byte	0x496
	.uleb128 0x7
	.4byte	0x5f6
	.byte	0
	.uleb128 0x8
	.ascii	"__TRISBbits_t\000"
	.byte	0x3
	.2byte	0x12f6
	.4byte	0x610
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12fe
	.4byte	0x76a
	.uleb128 0x5
	.ascii	"RB0\000"
	.byte	0x3
	.2byte	0x12ff
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB1\000"
	.byte	0x3
	.2byte	0x1300
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB2\000"
	.byte	0x3
	.2byte	0x1301
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB3\000"
	.byte	0x3
	.2byte	0x1302
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB4\000"
	.byte	0x3
	.2byte	0x1303
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB5\000"
	.byte	0x3
	.2byte	0x1304
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB6\000"
	.byte	0x3
	.2byte	0x1305
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB7\000"
	.byte	0x3
	.2byte	0x1306
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB8\000"
	.byte	0x3
	.2byte	0x1307
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB9\000"
	.byte	0x3
	.2byte	0x1308
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB10\000"
	.byte	0x3
	.2byte	0x1309
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB11\000"
	.byte	0x3
	.2byte	0x130a
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB12\000"
	.byte	0x3
	.2byte	0x130b
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB13\000"
	.byte	0x3
	.2byte	0x130c
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB14\000"
	.byte	0x3
	.2byte	0x130d
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB15\000"
	.byte	0x3
	.2byte	0x130e
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1310
	.4byte	0x784
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1311
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x12fd
	.4byte	0x798
	.uleb128 0x7
	.4byte	0x63a
	.uleb128 0x7
	.4byte	0x76a
	.byte	0
	.uleb128 0x8
	.ascii	"__PORTBbits_t\000"
	.byte	0x3
	.2byte	0x1313
	.4byte	0x784
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x131b
	.4byte	0x8fe
	.uleb128 0x5
	.ascii	"LATB0\000"
	.byte	0x3
	.2byte	0x131c
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB1\000"
	.byte	0x3
	.2byte	0x131d
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB2\000"
	.byte	0x3
	.2byte	0x131e
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB3\000"
	.byte	0x3
	.2byte	0x131f
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB4\000"
	.byte	0x3
	.2byte	0x1320
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB5\000"
	.byte	0x3
	.2byte	0x1321
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB6\000"
	.byte	0x3
	.2byte	0x1322
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB7\000"
	.byte	0x3
	.2byte	0x1323
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB8\000"
	.byte	0x3
	.2byte	0x1324
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB9\000"
	.byte	0x3
	.2byte	0x1325
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB10\000"
	.byte	0x3
	.2byte	0x1326
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB11\000"
	.byte	0x3
	.2byte	0x1327
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB12\000"
	.byte	0x3
	.2byte	0x1328
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB13\000"
	.byte	0x3
	.2byte	0x1329
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB14\000"
	.byte	0x3
	.2byte	0x132a
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB15\000"
	.byte	0x3
	.2byte	0x132b
	.4byte	0xcf
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x132d
	.4byte	0x918
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x132e
	.4byte	0xcf
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x131a
	.4byte	0x92c
	.uleb128 0x7
	.4byte	0x7ae
	.uleb128 0x7
	.4byte	0x8fe
	.byte	0
	.uleb128 0x8
	.ascii	"__LATBbits_t\000"
	.byte	0x3
	.2byte	0x1330
	.4byte	0x918
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.ascii	"long unsigned int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.ascii	"long int\000"
	.uleb128 0x9
	.byte	0x1
	.ascii	"pinMode\000"
	.byte	0x1
	.byte	0xe
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST0
	.byte	0x1
	.4byte	0x99e
	.uleb128 0xa
	.ascii	"pin\000"
	.byte	0x1
	.byte	0xe
	.4byte	0x83
	.4byte	.LLST1
	.uleb128 0xb
	.ascii	"mode\000"
	.byte	0x1
	.byte	0xe
	.4byte	0x83
	.byte	0x1
	.byte	0x55
	.byte	0
	.uleb128 0x9
	.byte	0x1
	.ascii	"digitalWrite\000"
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.4byte	.LLST2
	.byte	0x1
	.4byte	0x9e0
	.uleb128 0xa
	.ascii	"pin\000"
	.byte	0x1
	.byte	0x68
	.4byte	0x83
	.4byte	.LLST3
	.uleb128 0xb
	.ascii	"value\000"
	.byte	0x1
	.byte	0x68
	.4byte	0x83
	.byte	0x1
	.byte	0x55
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.ascii	"digitalRead\000"
	.byte	0x1
	.byte	0xad
	.byte	0x1
	.4byte	0x83
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST4
	.byte	0x1
	.4byte	0xa26
	.uleb128 0xb
	.ascii	"pin\000"
	.byte	0x1
	.byte	0xad
	.4byte	0x83
	.byte	0x1
	.byte	0x54
	.uleb128 0xd
	.ascii	"pinVal\000"
	.byte	0x1
	.byte	0xae
	.4byte	0x83
	.4byte	.LLST5
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.ascii	"main\000"
	.byte	0x1
	.byte	0xf7
	.byte	0x1
	.4byte	0xc8
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST6
	.byte	0x1
	.4byte	0xb60
	.uleb128 0xe
	.ascii	"outPin\000"
	.byte	0x1
	.2byte	0x110
	.4byte	0x83
	.4byte	.LLST7
	.uleb128 0xe
	.ascii	"modePin\000"
	.byte	0x1
	.2byte	0x111
	.4byte	0x83
	.4byte	.LLST8
	.uleb128 0xe
	.ascii	"testPin\000"
	.byte	0x1
	.2byte	0x112
	.4byte	0x83
	.4byte	.LLST9
	.uleb128 0xf
	.4byte	.LBB2
	.4byte	.LBE2
	.4byte	0xb2d
	.uleb128 0x10
	.ascii	"testVal\000"
	.byte	0x1
	.2byte	0x11a
	.4byte	0x83
	.byte	0x5
	.byte	0x3
	.4byte	testVal.6917
	.uleb128 0x10
	.ascii	"modeVal\000"
	.byte	0x1
	.2byte	0x11b
	.4byte	0x83
	.byte	0x5
	.byte	0x3
	.4byte	modeVal.6918
	.uleb128 0x10
	.ascii	"lastModeVal\000"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x83
	.byte	0x5
	.byte	0x3
	.4byte	lastModeVal.6919
	.uleb128 0x11
	.4byte	.LVL141
	.4byte	0x9e0
	.4byte	0xae9
	.uleb128 0x12
	.byte	0x1
	.byte	0x54
	.byte	0x4
	.byte	0x8d
	.sleb128 18
	.byte	0x94
	.byte	0x1
	.byte	0
	.uleb128 0x11
	.4byte	.LVL142
	.4byte	0x962
	.4byte	0xb04
	.uleb128 0x12
	.byte	0x1
	.byte	0x55
	.byte	0x1
	.byte	0x31
	.uleb128 0x12
	.byte	0x1
	.byte	0x54
	.byte	0x4
	.byte	0x8d
	.sleb128 16
	.byte	0x94
	.byte	0x1
	.byte	0
	.uleb128 0x11
	.4byte	.LVL143
	.4byte	0x9e0
	.4byte	0xb1a
	.uleb128 0x12
	.byte	0x1
	.byte	0x54
	.byte	0x4
	.byte	0x8d
	.sleb128 16
	.byte	0x94
	.byte	0x1
	.byte	0
	.uleb128 0x13
	.4byte	.LVL144
	.4byte	0x99e
	.uleb128 0x12
	.byte	0x1
	.byte	0x54
	.byte	0x4
	.byte	0x8d
	.sleb128 17
	.byte	0x94
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x11
	.4byte	.LVL139
	.4byte	0x962
	.4byte	0xb48
	.uleb128 0x12
	.byte	0x1
	.byte	0x55
	.byte	0x1
	.byte	0x30
	.uleb128 0x12
	.byte	0x1
	.byte	0x54
	.byte	0x4
	.byte	0x8d
	.sleb128 17
	.byte	0x94
	.byte	0x1
	.byte	0
	.uleb128 0x13
	.4byte	.LVL140
	.4byte	0x962
	.uleb128 0x12
	.byte	0x1
	.byte	0x55
	.byte	0x1
	.byte	0x31
	.uleb128 0x12
	.byte	0x1
	.byte	0x54
	.byte	0x4
	.byte	0x8d
	.sleb128 18
	.byte	0x94
	.byte	0x1
	.byte	0
	.byte	0
	.uleb128 0x14
	.ascii	"ANSELAbits\000"
	.byte	0x3
	.2byte	0x1223
	.ascii	"ANSELA\000"
	.4byte	0xb7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x17c
	.uleb128 0x14
	.ascii	"TRISAbits\000"
	.byte	0x3
	.2byte	0x1235
	.ascii	"TRISA\000"
	.4byte	0xb9b
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x234
	.uleb128 0x14
	.ascii	"PORTAbits\000"
	.byte	0x3
	.2byte	0x1247
	.ascii	"PORTA\000"
	.4byte	0xbba
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x2dc
	.uleb128 0x14
	.ascii	"LATAbits\000"
	.byte	0x3
	.2byte	0x1259
	.ascii	"LATA\000"
	.4byte	0xbd7
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x38e
	.uleb128 0x14
	.ascii	"ANSELBbits\000"
	.byte	0x3
	.2byte	0x12da
	.ascii	"ANSELB\000"
	.4byte	0xbf8
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x47f
	.uleb128 0x14
	.ascii	"TRISBbits\000"
	.byte	0x3
	.2byte	0x12f7
	.ascii	"TRISB\000"
	.4byte	0xc17
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x624
	.uleb128 0x14
	.ascii	"PORTBbits\000"
	.byte	0x3
	.2byte	0x1314
	.ascii	"PORTB\000"
	.4byte	0xc36
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x798
	.uleb128 0x14
	.ascii	"LATBbits\000"
	.byte	0x3
	.2byte	0x1331
	.ascii	"LATB\000"
	.4byte	0xc53
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.4byte	0x92c
	.byte	0
	.section	.debug_abbrev,info
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0x8
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1b
	.uleb128 0x8
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0xa
	.uleb128 0x2111
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0x8
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,info
.Ldebug_loc0:
.LLST0:
	.4byte	.LFB7-.Ltext0
	.4byte	.LCFI0-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LCFI0-.Ltext0
	.4byte	.LCFI2-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 8
	.4byte	.LCFI2-.Ltext0
	.4byte	.LCFI3-.Ltext0
	.2byte	0x2
	.byte	0x8e
	.sleb128 8
	.4byte	.LCFI3-.Ltext0
	.4byte	.LCFI4-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 8
	.4byte	.LCFI4-.Ltext0
	.4byte	.LFE7-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LVL0-.Ltext0
	.4byte	.LVL1-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL1-.Ltext0
	.4byte	.LVL2-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL2-.Ltext0
	.4byte	.LVL3-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL3-.Ltext0
	.4byte	.LVL4-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL4-.Ltext0
	.4byte	.LVL5-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL5-.Ltext0
	.4byte	.LVL6-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL6-.Ltext0
	.4byte	.LVL7-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL7-.Ltext0
	.4byte	.LVL8-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL8-.Ltext0
	.4byte	.LVL9-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL9-.Ltext0
	.4byte	.LVL10-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL10-.Ltext0
	.4byte	.LVL11-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL11-.Ltext0
	.4byte	.LVL12-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL12-.Ltext0
	.4byte	.LVL13-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL13-.Ltext0
	.4byte	.LVL14-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL14-.Ltext0
	.4byte	.LVL15-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL15-.Ltext0
	.4byte	.LVL16-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL16-.Ltext0
	.4byte	.LVL17-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL17-.Ltext0
	.4byte	.LVL18-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL18-.Ltext0
	.4byte	.LVL19-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL19-.Ltext0
	.4byte	.LVL20-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL20-.Ltext0
	.4byte	.LVL21-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL21-.Ltext0
	.4byte	.LVL22-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL22-.Ltext0
	.4byte	.LVL23-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL23-.Ltext0
	.4byte	.LVL24-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL24-.Ltext0
	.4byte	.LVL25-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL25-.Ltext0
	.4byte	.LVL26-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL26-.Ltext0
	.4byte	.LVL27-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL27-.Ltext0
	.4byte	.LVL28-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL28-.Ltext0
	.4byte	.LVL29-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL29-.Ltext0
	.4byte	.LVL30-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL30-.Ltext0
	.4byte	.LVL31-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL31-.Ltext0
	.4byte	.LVL32-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL32-.Ltext0
	.4byte	.LVL33-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL33-.Ltext0
	.4byte	.LVL34-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL34-.Ltext0
	.4byte	.LVL35-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL35-.Ltext0
	.4byte	.LVL36-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL36-.Ltext0
	.4byte	.LVL37-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL37-.Ltext0
	.4byte	.LVL38-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL38-.Ltext0
	.4byte	.LVL39-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL39-.Ltext0
	.4byte	.LVL40-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL40-.Ltext0
	.4byte	.LVL41-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL41-.Ltext0
	.4byte	.LVL42-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL42-.Ltext0
	.4byte	.LVL43-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL43-.Ltext0
	.4byte	.LFE7-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LFB8-.Ltext0
	.4byte	.LCFI5-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LCFI5-.Ltext0
	.4byte	.LCFI7-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 8
	.4byte	.LCFI7-.Ltext0
	.4byte	.LCFI8-.Ltext0
	.2byte	0x2
	.byte	0x8e
	.sleb128 8
	.4byte	.LCFI8-.Ltext0
	.4byte	.LCFI9-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 8
	.4byte	.LCFI9-.Ltext0
	.4byte	.LFE8-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	0
	.4byte	0
.LLST3:
	.4byte	.LVL44-.Ltext0
	.4byte	.LVL45-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL45-.Ltext0
	.4byte	.LVL46-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL46-.Ltext0
	.4byte	.LVL47-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL47-.Ltext0
	.4byte	.LVL48-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL48-.Ltext0
	.4byte	.LVL49-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL49-.Ltext0
	.4byte	.LVL50-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL50-.Ltext0
	.4byte	.LVL51-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL51-.Ltext0
	.4byte	.LVL52-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL52-.Ltext0
	.4byte	.LVL53-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL53-.Ltext0
	.4byte	.LVL54-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL54-.Ltext0
	.4byte	.LVL55-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL55-.Ltext0
	.4byte	.LVL56-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL56-.Ltext0
	.4byte	.LVL57-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL57-.Ltext0
	.4byte	.LVL58-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL58-.Ltext0
	.4byte	.LVL59-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL59-.Ltext0
	.4byte	.LVL60-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL60-.Ltext0
	.4byte	.LVL61-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL61-.Ltext0
	.4byte	.LVL62-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL62-.Ltext0
	.4byte	.LVL63-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL63-.Ltext0
	.4byte	.LVL64-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL64-.Ltext0
	.4byte	.LVL65-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL65-.Ltext0
	.4byte	.LVL66-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL66-.Ltext0
	.4byte	.LVL67-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL67-.Ltext0
	.4byte	.LVL68-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL68-.Ltext0
	.4byte	.LVL69-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL69-.Ltext0
	.4byte	.LVL70-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL70-.Ltext0
	.4byte	.LVL71-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL71-.Ltext0
	.4byte	.LVL72-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL72-.Ltext0
	.4byte	.LVL73-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL73-.Ltext0
	.4byte	.LVL74-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL74-.Ltext0
	.4byte	.LVL75-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL75-.Ltext0
	.4byte	.LVL76-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL76-.Ltext0
	.4byte	.LVL77-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL77-.Ltext0
	.4byte	.LVL78-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL78-.Ltext0
	.4byte	.LVL79-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL79-.Ltext0
	.4byte	.LVL80-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL80-.Ltext0
	.4byte	.LVL81-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL81-.Ltext0
	.4byte	.LVL82-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL82-.Ltext0
	.4byte	.LVL83-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL83-.Ltext0
	.4byte	.LVL84-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL84-.Ltext0
	.4byte	.LVL85-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL85-.Ltext0
	.4byte	.LVL86-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	.LVL86-.Ltext0
	.4byte	.LVL87-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	.LVL87-.Ltext0
	.4byte	.LFE8-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST4:
	.4byte	.LFB9-.Ltext0
	.4byte	.LCFI10-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LCFI10-.Ltext0
	.4byte	.LCFI12-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 16
	.4byte	.LCFI12-.Ltext0
	.4byte	.LCFI13-.Ltext0
	.2byte	0x2
	.byte	0x8e
	.sleb128 16
	.4byte	.LCFI13-.Ltext0
	.4byte	.LCFI14-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 16
	.4byte	.LCFI14-.Ltext0
	.4byte	.LFE9-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	0
	.4byte	0
.LLST5:
	.4byte	.LVL89-.Ltext0
	.4byte	.LVL90-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL90-.Ltext0
	.4byte	.LVL91-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL91-.Ltext0
	.4byte	.LVL92-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL92-.Ltext0
	.4byte	.LVL93-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL93-.Ltext0
	.4byte	.LVL94-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL94-.Ltext0
	.4byte	.LVL95-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL95-.Ltext0
	.4byte	.LVL96-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL96-.Ltext0
	.4byte	.LVL97-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL97-.Ltext0
	.4byte	.LVL98-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL98-.Ltext0
	.4byte	.LVL99-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL99-.Ltext0
	.4byte	.LVL100-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL100-.Ltext0
	.4byte	.LVL101-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL101-.Ltext0
	.4byte	.LVL102-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL102-.Ltext0
	.4byte	.LVL103-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL103-.Ltext0
	.4byte	.LVL104-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL104-.Ltext0
	.4byte	.LVL105-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL105-.Ltext0
	.4byte	.LVL106-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL106-.Ltext0
	.4byte	.LVL107-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL107-.Ltext0
	.4byte	.LVL108-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL108-.Ltext0
	.4byte	.LVL109-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL109-.Ltext0
	.4byte	.LVL110-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL110-.Ltext0
	.4byte	.LVL111-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL111-.Ltext0
	.4byte	.LVL112-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL112-.Ltext0
	.4byte	.LVL113-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL113-.Ltext0
	.4byte	.LVL114-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL114-.Ltext0
	.4byte	.LVL115-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL115-.Ltext0
	.4byte	.LVL116-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL116-.Ltext0
	.4byte	.LVL117-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL117-.Ltext0
	.4byte	.LVL118-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL118-.Ltext0
	.4byte	.LVL119-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL119-.Ltext0
	.4byte	.LVL120-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL120-.Ltext0
	.4byte	.LVL121-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL121-.Ltext0
	.4byte	.LVL122-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL122-.Ltext0
	.4byte	.LVL123-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL123-.Ltext0
	.4byte	.LVL124-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL124-.Ltext0
	.4byte	.LVL125-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL125-.Ltext0
	.4byte	.LVL126-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL126-.Ltext0
	.4byte	.LVL127-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL127-.Ltext0
	.4byte	.LVL128-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL128-.Ltext0
	.4byte	.LVL129-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL129-.Ltext0
	.4byte	.LVL130-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL130-.Ltext0
	.4byte	.LVL131-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL131-.Ltext0
	.4byte	.LVL132-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LVL132-.Ltext0
	.4byte	.LFE9-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 -16
	.4byte	0
	.4byte	0
.LLST6:
	.4byte	.LFB10-.Ltext0
	.4byte	.LCFI15-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 0
	.4byte	.LCFI15-.Ltext0
	.4byte	.LCFI17-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 32
	.4byte	.LCFI17-.Ltext0
	.4byte	.LFE10-.Ltext0
	.2byte	0x2
	.byte	0x8e
	.sleb128 32
	.4byte	0
	.4byte	0
.LLST7:
	.4byte	.LVL133-.Ltext0
	.4byte	.LVL134-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL134-.Ltext0
	.4byte	.LFE10-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 17
	.4byte	0
	.4byte	0
.LLST8:
	.4byte	.LVL135-.Ltext0
	.4byte	.LVL136-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL136-.Ltext0
	.4byte	.LFE10-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 18
	.4byte	0
	.4byte	0
.LLST9:
	.4byte	.LVL137-.Ltext0
	.4byte	.LVL138-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL138-.Ltext0
	.4byte	.LFE10-.Ltext0
	.2byte	0x2
	.byte	0x8d
	.sleb128 16
	.4byte	0
	.4byte	0
	.section	.debug_aranges,info
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,info
.Ldebug_line0:
	.section	.debug_str,info
	.ident	"GCC: (Microchip Technology) 4.8.3 MPLAB XC32 Compiler v2.41"
# Begin MCHP vector dispatch table
# End MCHP vector dispatch table
# Microchip Technology PIC32 MCU configuration words
# Configuration word @ 0xbfc00bfc
	.section	.config_BFC00BFC, code, keep, address(0xBFC00BFC)
	.type	__config_BFC00BFC, @object
	.size	__config_BFC00BFC, 4
__config_BFC00BFC:
	.word	0x7FFFFFFB
# Configuration word @ 0xbfc00bf8
	.section	.config_BFC00BF8, code, keep, address(0xBFC00BF8)
	.type	__config_BFC00BF8, @object
	.size	__config_BFC00BF8, 4
__config_BFC00BF8:
	.word	0xFF74DF59
# Configuration word @ 0xbfc00bf4
	.section	.config_BFC00BF4, code, keep, address(0xBFC00BF4)
	.type	__config_BFC00BF4, @object
	.size	__config_BFC00BF4, 4
__config_BFC00BF4:
	.word	0xFFF9FFD9
# Configuration word @ 0xbfc00bf0
	.section	.config_BFC00BF0, code, keep, address(0xBFC00BF0)
	.type	__config_BFC00BF0, @object
	.size	__config_BFC00BF0, 4
__config_BFC00BF0:
	.word	0xCFFFFFFF
